# Projet MIPS ESISAR 2019

this is the repo for ESISAR 3A MIPS project by Anton DUMAS and Gael Dezanneau

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites

* GNU GCC for C99 or above
* GNU Make
* ncurses C Library
* Bash

## Project Infos

see Documentation in CR directory for operation

## Built With

* [GNU GCC](https://gcc.gnu.org/onlinedocs/) - C compiler
* [NCURSES](http://www.cs.ukzn.ac.za/~hughm/os/notes/ncurses.html) - a graphical Library for std terminal


## Authors

* **Anton Dumas** - *Initial work* - [roscov](https://gitlab.com/antondumas)
* **Gael Dezanneau** - *Initial work* 

## License

Licence au fillot récursif

## Acknowledgments

* si tu peux comprendre le code tu le mérite
* pas de **SAV** ici
