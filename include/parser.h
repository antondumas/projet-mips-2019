/*
--------------------------------------------------------------------------------------------------------
source:parser.h, parser.c
author:Anton Dumas, Gael Dezanneau
projet:projet MIPS
organization:INP ESISAR
comments:header description convertisseur ASM to Opcode
--------------------------------------------------------------------------------------------------------
*/

#ifndef _PARSER_H_
#define _PARSER_H_

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "graph.h"

#define CHARLECT 200

typedef struct construct {

    char instruction[8];
    char op1[10];
    char op2[10];
    char op3[10];
    char notainst ;
    char isimmediat ;

}construct;

void fill_struct (char *instruct, construct *output);

int parse_file ( char *fichier, char *outfichier, unsigned char *tablexa );

unsigned int parse_input ( char *input, construct *output );

unsigned int OpToNum(char *input);

unsigned int InstToHex ( construct *input );

unsigned int ExtractBase(char *input);

unsigned int ExtractOffset(char *input);

#endif