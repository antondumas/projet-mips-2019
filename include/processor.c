#include "processor.h"

void ProcessorInit(core *processor){    //initialisation du processeur

    (*processor).PC = 0;
    (*processor).IR = 0;
    (*processor).HI = 0;
    (*processor).LO = 0;

    for(int i=0 ; i<=32; i++ ){

        (*processor).registers[i] = 0;

    }

}

int ProcessorOperation(core *processor, char *memory, int mode){

    unsigned int tempIR, tempPC, tempREG;
    long int temp;
    int out=0,newtemp;
    //tempPC=(*processor).PC + memory;
    //mvwprintw(CMD, 11, 1, "%08x", (memory[processor->PC]<<24)+(memory[processor->PC + 1]<<16)+/*(memory[processor->PC + 2]<<8)+ */memory[processor->PC + 3]);
    if(mode == 0){
        // tempIR = (*(memory+tempPC)<<24)+(*(memory+tempPC+1)<<16)+(*(memory+tempPC+2)<<8)+*(memory+tempPC+3);
        // processor->IR = (memory[processor->PC]<<24)+(memory[processor->PC + 1]<<16)+(memory[processor->PC + 2]<<8)+memory[processor->PC + 3];
        processor->IR = memory[processor->PC + 3]&0xff;//memory[processor->PC + 3];
        processor->IR += (memory[processor->PC + 2]<<8)&0xff00;
        processor->IR += (memory[processor->PC + 1]<<16)&0xff0000;
        processor->IR += (memory[processor->PC]<<24)&0xff000000;
    } 
    //mvwprintw(CMD, 10, 1, "%08x", (*processor).IR);
    (*processor).PC += 4;
    tempIR = (*processor).IR;
    //(*processor).IR = tempIR;

    if((tempIR & 0xfc000000) != 0 ){
        
        switch((tempIR & 0xfc000000) >> 26){

            case 0b001000: (*processor).registers[(tempIR >> 16)&0x1f] = (*processor).registers[(tempIR >> 21)&0x1f] + (short int)(tempIR & 0xffff);                                       // ADDI
            break;
            case 0b000100: if((*processor).registers[(tempIR >> 16)&0x1f]==(*processor).registers[(tempIR >> 21)&0x1f]) (*processor).PC += (short int)((tempIR&0xffff)<<2) ;    // BEQ offset negatif a revoir
            break;
            case 0b000111: if((*processor).registers[0]<(*processor).registers[(tempIR >> 21)&0x1f]) (*processor).PC += (short int)((tempIR&0xffff)<<2) ;                       // BGTZ offset negatif a revoir
            break;
            case 0b000110: if((*processor).registers[0]>=(*processor).registers[(tempIR >> 21)&0x1f]) (*processor).PC += (short int)((tempIR&0xffff)<<2) ;                      // BLEZ offset negatif a revoir
            break;
            case 0b000101: if((*processor).registers[(tempIR >> 16)&0x1f]!=(*processor).registers[(tempIR >> 21)&0x1f]) (*processor).PC += (short int)((tempIR&0xffff)<<2) ;    // BNE offset negatif a revoir
            break;
            case 0b000010: (*processor).PC = ((*processor).PC & 0xf0000000) + (tempIR&0x3ffffff)<<2;                                                                            // J
            break;
            case 0b000011: (*processor).registers[31] = (*processor).PC + 8;                                                                                                        // JAL
                           (*processor).PC = ((*processor).PC & 0xf0000000) + (tempIR&0x3ffffff)<<2;
            break;
            case 0b001111: (*processor).registers[(tempIR >> 16)&0x1f] =  (tempIR & 0xffff) << 16;                                                                              // LUI
            break;
            case 0b100011: MemoryOperation(memory, processor->registers[(tempIR >> 21)&0x1f]+(short int)(tempIR&0xffff), &((*processor).registers[(tempIR >> 16)&0x1f]),'r');        // LW
            break;
            case 0b101011: MemoryOperation(memory, processor->registers[(tempIR >> 21)&0x1f]+(short int)(tempIR&0xffff), &((*processor).registers[(tempIR >> 16)&0x1f]),'w');        // SW
            break;
            default: out = 24;
            break;
        }

    }else{

         switch((tempIR & 0x0000003f)){

            case 0b100000:  if ((processor->registers[(tempIR >> 21)&0x1f] > 0) && (processor->registers[(tempIR >> 16)&0x1f] > (0xffffffff-processor->registers[(tempIR >> 21)&0x1f]))) out = 48;
                            (*processor).registers[(tempIR >> 11)&0x1f]=(*processor).registers[(tempIR >> 21)&0x1f] + (*processor).registers[(tempIR >> 16)&0x1f];  // ADD
            break;
            case 0b100100: (*processor).registers[(tempIR >> 11)&0x1f]=(*processor).registers[(tempIR >> 21)&0x1f] & (*processor).registers[(tempIR >> 16)&0x1f];   // AND
            break;
            case 0b011010: if(processor->registers[(tempIR >> 16)&0x1f]!=0){
                                (*processor).LO = (*processor).registers[(tempIR >> 21)&0x1f] / (*processor).registers[(tempIR >> 16)&0x1f];                        // DIV 
                                (*processor).HI = (*processor).registers[(tempIR >> 21)&0x1f] % (*processor).registers[(tempIR >> 16)&0x1f];
                            }else out = 60;
            break;
            case 0b001000: (*processor).PC = (*processor).registers[(tempIR >> 21)&0x1f]; // JR
            break;
            case 0b010000: (*processor).registers[(tempIR >> 11)&0x1f] = (*processor).HI;                                                                           // MFHI
            break;
            case 0b010010: (*processor).registers[(tempIR >> 11)&0x1f] = (*processor).LO;                                                                           // MFLO
            break;
            case 0b011000: temp=(*processor).registers[(tempIR >> 21)&0x1f] * (*processor).registers[(tempIR >> 16)&0x1f];                                          // MULT 
                           (*processor).LO = temp & 0xffffffff;
                           (*processor).HI = (temp>>32) & 0xffffffff;
            break;
            case 0b000000: (*processor).registers[(tempIR >> 11)&0x1f]= (*processor).registers[(tempIR >> 16)&0x1f] << ((tempIR >> 6)&0x1f);                        // SLL
            break;
            case 0b100101: (*processor).registers[(tempIR >> 11)&0x1f]=(*processor).registers[(tempIR >> 21)&0x1f] | (*processor).registers[(tempIR >> 16)&0x1f];   // OR
            break;
            case 0b000010:  if((tempIR & 0x200000) == 0){
                                (*processor).registers[(tempIR >> 11)&0x1f] =  (*processor).registers[(tempIR >> 16)&0x1f] >> ((tempIR >> 6)&0x1f) ;                //  SRL
                            }else{
                                tempREG = (*processor).registers[(tempIR >> 16)&0x1f];
                                (*processor).registers[(tempIR >> 11)&0x1f] = tempREG << (32 - ((tempIR >> 6)&0x1f));                                               // ROTR
                                (*processor).registers[(tempIR >> 11)&0x1f] += tempREG >> ((tempIR >> 6)&0x1f) ;
                            }
            break;
            case 0b101010: if((*processor).registers[(tempIR >> 16)&0x1f]>(*processor).registers[(tempIR >> 21)&0x1f]){                                             // SLT
                               (*processor).registers[(tempIR >> 11)&0x1f]=1; 
                           }else{
                               (*processor).registers[(tempIR >> 11)&0x1f]=0;
                           }
            break;
            case 0b100010: (*processor).registers[(tempIR >> 11)&0x1f]=(*processor).registers[(tempIR >> 21)&0x1f] - (*processor).registers[(tempIR >> 16)&0x1f];   // SUB
            break;
            case 0b001100: out = 32; // SYSCALL
            break;
            case 0b100110: (*processor).registers[(tempIR >> 11)&0x1f]=(*processor).registers[(tempIR >> 21)&0x1f] ^ (*processor).registers[(tempIR >> 16)&0x1f];   // XOR
            break;
            default: out = 24;
            break;
        }

    }

    (*processor).registers[0] = 0;

    return out;

}

int MemoryOperation(char *memory, int addr, int *data, char op){

    int temp;

    switch(op){

        case 'r':   temp = memory[addr + 3]&0xff;//memory[processor->PC + 3];
                    temp += (memory[addr + 2]<<8)&0xff00;
                    temp += (memory[addr + 1]<<16)&0xff0000;
                    temp += (memory[addr]<<24)&0xff000000;
                    *data = temp;
        break;
        case 'w':   *(memory + addr)=(*data>>24);//&0xff;
                    *(memory + addr + 1)=(*data>>16);//&0xff;
                    *(memory + addr + 2)=(*data>>8);//&0xff;
                    *(memory + addr + 3)=(*data);
        break;
        default:;
    }

    return temp;

}

//int offsethandle()