/*
--------------------------------------------------------------------------------------------------------
source:processor.h, processor.c
author:Anton Dumas
projet:projet MIPS
organization:INP ESISAR
comments:header description architecture processeur MIPS
--------------------------------------------------------------------------------------------------------
*/
#ifndef _PROCESSOR_H_
#define _PROCESSOR_H_

typedef struct core {                       /*structure décrivant le processeur*/

    int PC;
    int IR;
    int HI;
    int LO;
    int registers[32];

}core;

void ProcessorInit(core *processor);

int ProcessorOperation(core *processor, char *memory, int mode);

int MemoryOperation(char *memory, int addr, int *data, char op);

// char MEMORY[800];

#endif