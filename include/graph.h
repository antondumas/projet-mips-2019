/*
--------------------------------------------------------------------------------------------------------
source:graph.h, graph.c
author:Anton Dumas
projet:projet MIPS
organization:INP ESISAR
comments:header description fonctions liées a l'interface graphique
--------------------------------------------------------------------------------------------------------
*/

#ifndef _GRAPH_H_
#define _GRAPH_H_

#include <curses.h>
#include "parser.h"
#include "processor.h"

#define REG_COLOR   1
#define PC_COLOR    2
#define HL_COLOR    3
#define MEM_COLOR   4
#define CMD_COLOR   5
#define IR_COLOR    6

WINDOW *regs, *PC, *HI, *LO, *MEM, *CMD, *IR, *wintest;

void initInterface();

void showInterface();

void showRegs(core *proc);

void showMemory(char *memory, unsigned int startaddr);

int handleCMD(int *lns);

void AhA(int oulala);


#endif