#include "parser.h"

void fill_struct (char *instruct, construct *output){

    char temp=0, cnt=0, cnts=0, off[10], pasoff[10];
    int temphex, z=0, zb=0, outoff=0;

    (*output).instruction[0] = '\0';
    (*output).op1[0] = '\0';
    (*output).op2[0] = '\0';
    (*output).op3[0] = '\0';

    (*output).notainst = 0;

    //while((*(instruct+cnt) < 'A' || *(instruct+cnt) > 'Z') && (*output).notainst == 0 ){
    //while((*(instruct+cnt) < 'a' || *(instruct+cnt) > 'z') && (*output).notainst == 0 ){
    while((*(instruct+cnt) < 'A' || (*(instruct+cnt) < 'a' && *(instruct+cnt) > 'Z') || *(instruct+cnt) > 'z') && (*output).notainst == 0 ){ 
    //while(((*(instruct+cnt)<'a' || *(instruct+cnt)>'z') && (*(instruct+cnt)<'A' || *(instruct+cnt)>'Z')) && (*output).notainst == 0 ){
        //if(*(instruct+cnt) >= 'a' && *(instruct+cnt) <= 'z')break;
        if(*(instruct+cnt) == '#' || *(instruct+cnt) == '\0'){

            (*output).notainst = 1;
            break;

        }
        //printf("%c %d", *(instruct+cnt),cnt);
        cnt++;
    }

    //cnt--;

    while(*(instruct+cnt) != '\0' && *(instruct+cnt) != '#' && (*output).notainst != 1 ){
        
        if( *(instruct+cnt) != ' ' && *(instruct+cnt) != ',' && *(instruct+cnt) != '\n'){
            
            switch(temp){
                case 0 : if(*(instruct+cnt) >= 'a' && *(instruct+cnt) <= 'z')(*output).instruction[cnts] = *(instruct+cnt) - 32;
                         else (*output).instruction[cnts] = *(instruct+cnt);
                         (*output).instruction[cnts+1] = '\0';
                break;
                case 1 : (*output).op1[cnts] = *(instruct+cnt);
                         (*output).op1[cnts+1] = '\0';
                break;
                case 2 : (*output).op2[cnts] = *(instruct+cnt);
                         (*output).op2[cnts+1] = '\0';
                break;
                case 3 : (*output).op3[cnts] = *(instruct+cnt);
                         (*output).op3[cnts+1] = '\0';
                break;
            }

            cnts++;

        }else{

            if(*(instruct+cnt+1) != ' ') temp++;
            cnts=0;

        }

        cnt++;

    }

    if((*output).op1[0]=='0' && ((*output).op1[1]=='x' || (*output).op1[1]=='X')){
        
        (*output).op1[1]='X';
        sscanf((*output).op1, "0X%x", &temphex);
        sprintf((*output).op1, "%d", temphex);

    }if((*output).op2[0]=='0' && ((*output).op2[1]=='x' || (*output).op2[1]=='X')){
        
        (*output).op2[1]='X';
        while((*output).op2[z]!='\0'){

            if((*output).op2[z]=='(') outoff=1;
            if(outoff == 0){
                off[z]=(*output).op2[z];
                off[z+1]='\0';
            }else{
                pasoff[zb]=(*output).op2[z];  
                pasoff[++zb]='\0';
            }
            z++;

        }
        (*output).op2[0]='\0';
        sscanf(off, "0X%x", &temphex);
        sprintf((*output).op2, "%hi", (short int)(temphex&0xffff));
        strcat((*output).op2,pasoff);


    }if((*output).op3[0]=='0' && ((*output).op3[1]=='x' || (*output).op3[1]=='X')){
        
        (*output).op3[1]='X';
        sscanf((*output).op3, "0X%x", &temphex);
        sprintf((*output).op3, "%d", (short int)(temphex&0xffff));

    }

    if((*output).instruction[0] == 'B' || (*output).instruction[0] == 'L' || ((*output).instruction[3] == 'I' && (*output).instruction[0] == 'A') || (*output).instruction[1] == 'W' ){

        (*output).isimmediat = 1;

    }else{

        (*output).isimmediat = 0;

    }

}

int parse_file ( char *fichier, char *outfichier, unsigned char *tablexa ){

    FILE* ASM_source = NULL;
    FILE* HEX_out = NULL;
 
    ASM_source = fopen(fichier, "r");   // opening ASM source file
    HEX_out = fopen(outfichier, "w");      // opening output file

    if (ASM_source != NULL){    // if file exist and readeable

        char instruct[CHARLECT] ;
        construct instdef;
        char scanerror=0;
        int tabcnt=0, temp;

        while( scanerror != EOF ){  //do untill file ends

            if( fgets( instruct, CHARLECT, ASM_source) != NULL ){
            
                temp=parse_input( instruct, &instdef ); // conversion of string to HEX opcode
                if( instdef.notainst != 1){
                    /**(tablexa+tabcnt)=temp;
                    fprintf(HEX_out, "%08x\n", temp);
                    tabcnt += 4;*/
                    fprintf(HEX_out, "%08x\n", temp);
                    *(tablexa+tabcnt)=(temp>>24);//&0xff;
                    tabcnt++;
                    *(tablexa+tabcnt)=(temp>>16);//&0xff;
                    tabcnt++;
                    *(tablexa+tabcnt)=(temp>>8);//&0xff;
                    tabcnt++;
                    *(tablexa+tabcnt)=(temp);//&0xff;
                    tabcnt++;

                        /*printf("\nHEX OPcode: 0x%08x\n", temp);                   // test des valeurs
                        printf("instruction:%s\n", instdef.instruction);
                        printf("Operande1:  %s\n", instdef.op1);
                        printf("Operande2:  %s\n", instdef.op2);
                        printf("Operande3:  %s\n", instdef.op3);
                        printf("IMMEDIAT:   %d\n", instdef.isimmediat);*/

                }

            }else{

                scanerror = EOF;

            }

        }

        fclose(ASM_source);
        fclose(HEX_out);

        return 0;

    }else{

        //printf("!ERROR! file opening not possible \n");
        return -1;

    }

}

unsigned int parse_input ( char *input, construct *instdef ){

    fill_struct( input, instdef );  // formalisation de la structure pour la conversion en opcode Hexa
    //mvwprintw(CMD, 5, 1, "%s",instdef->op1);
    return InstToHex( instdef );  //fonction struct to HEX Opcode

}

unsigned int InstToHex ( construct *input ){

    char ASM[26][8]={
        "ADD",
        "ADDI",
        "AND",
        "BEQ",
        "BGTZ",
        "BLEZ",
        "BNE",
        "DIV",
        "J",
        "JAL",
        "JR",
        "LUI",
        "LW",
        "MFHI",
        "MFLO",
        "MULT",
        "NOP",
        "OR",
        "ROTR",
        "SLL",
        "SLT",
        "SRL",
        "SUB",
        "SW",
        "SYSCALL",
        "XOR"
    };

    unsigned int Hex[26] = {
        0x00000020,
        0x20000000,
        0x00000024,
        0x10000000,
        0x1C000000,
        0x18000000,
        0x14000000,
        0x0000001A,
        0x08000000,
        0x0C000000,
        0x00000008,
        0x3C000000,
        0x8C000000,
        0x00000010,
        0x00000012,
        0x00000018,
        0x00000000,
        0x00000025,
        0x00200002,
        0x00000000,
        0x0000002A,
        0x00000002,
        0x00000022,
        0xAC000000,
        0x0000000C,
        0x00000026
    };

    unsigned int  valeur = 0;
	int k;
	/*traitement opcode*/
	/*printf("%s",input->instruction);*/
	for(k=0;k<26;k++)
	{
		/*printf("\n%s\t%08x",ASM[k],Hex[k]);*/
		if(!strcmp(ASM[k],input->instruction))
		{
			valeur += Hex[k];
		}
	}
	if(valeur > 0x00FF0000) /*======instruction classique======*/
	{
		if(input->isimmediat)/*traitement des operandes immédiats*/
		{
			if(valeur == 0x3C000000)/*LUI*/
			{
				valeur+= atoi(input->op2) & 0xffff; /*op2 immediate*/
				valeur+= OpToNum(input->op1) <<16; /*op1*/
			}
			else if(valeur == 0x8C000000 || valeur == 0xAC000000)/*LW ou SW*/
			{
				valeur+= OpToNum(input->op1) <<16; /*op1*/
				valeur+= ExtractBase(input->op2) <<21; /*op2*/
				valeur+= ExtractOffset(input->op2); /*op2*/
			}
			else if(valeur == 0x10000000 || valeur==0x14000000)/*BEQ ou BNEQ*/
			{
				valeur+= atoi(input->op3) & 0xffff; /*op3 immediate*/
				valeur+= OpToNum(input->op2) <<16; /*op2*/
				valeur+= OpToNum(input->op1) <<21; /*op1*/
			}
			else if(valeur==0x1C000000 || valeur==0x18000000)/*all others Branch instructions*/
			{
				valeur+= atoi(input->op2) & 0xffff; /*op2 immediate*/
				valeur+= OpToNum(input->op1) <<21; /*op1*/
			}
			else /*classic immediate  instruction*/
			{
				valeur+= atoi(input->op3) & 0xffff; /*op3 immediate*/
				valeur+= OpToNum(input->op2) <<21; /*op2*/
				valeur+= OpToNum(input->op1) <<16; /*op1*/
			}
		}else if(valeur==0x08000000 || valeur==0x0C000000){/*J et JAL*/
            
            valeur += (atoi(input->op1)&0x03FFFFFF); /*op1 sur 26 bits*/
            
        }
		else
		{
			valeur+= OpToNum(input->op3) <<21; /*op3*/
			valeur+= OpToNum(input->op2) <<16; /*op2*/
			valeur+= OpToNum(input->op1) <<11; /*op1*/
		}
	}
	else /*======instruction speciale======*/
	{
		if(valeur == 0x0000001A || valeur == 0x00000018 || valeur == 0x00000008)/*DIV ou MULT ou JR */
		{
			valeur+= OpToNum(input->op1) <<21; /*op1*/
			valeur+= OpToNum(input->op2) <<16; /*op2*/
		}
		else if(valeur==0x00000010 || valeur==0x00000012)/*MFLO  ou MFHI*/
		{
			valeur+= OpToNum(input->op1) <<11; /*op1*/
		}
		else if(valeur==0x00000000 || valeur==0x00000002|| valeur==0x00200002)/*SLL, ROTR, SRL, NOP (nop traité quand meme : operandes = 0 does not matter)*/
		{
			valeur+= OpToNum(input->op1) <<11; /*op1*/
			valeur+= OpToNum(input->op2) <<16; /*op2*/
			valeur+= (atoi(input->op3)&0x0000001F) <<6; /*op3*/
		}
		else/*AND, OR, SLT, SUB, XOR*/
		{
			/*traitement des operandes*/
			valeur+= OpToNum(input->op3) <<16; /*op3*/
			valeur+= OpToNum(input->op2) <<21; /*op2*/
			valeur+= OpToNum(input->op1) <<11; /*op1*/
		}
	}
	return(valeur);

}

unsigned int OpToNum(char *input)
{
    
    char RegMnem[32][6] = {
        "$zero",
        "$at",
        "$v0",
        "v1",
        "$a0",
        "$a1",
        "$a2",
        "$a3",
        "$t0",
        "$t1",
        "$t2",
        "$t3",
        "$t4",
        "$t5",
        "$t6",
        "$t7",
        "$s0",
        "$s1",
        "$s2",
        "$s3",
        "$s4",
        "$s5",
        "$s6",
        "$s7",
        "$t8",
        "$t9",
        "$k0",
        "$k1",
        "$gp",
        "$sp",
        "$fp",
        "$ra"
    };

    int k;
    unsigned int valeur = 0;
    /*creation d'une chaine sans $*/
    char moinsdollar[7];
    strcpy(moinsdollar,input);
    /*on enleve le $*/
    moinsdollar[0] = '0';
    /*on rempli la valeur de sortie (overwrite si mnemonique)*/
    valeur = atoi(moinsdollar);
    /*traitement de la valeurr*/
    for(k=0;k<32;k++)
    {
   	 if(!strcmp(RegMnem[k],input)){valeur=k;}
    }
    return(valeur);
}

unsigned int ExtractOffset(char *input)
{
        int k=0;
        char Offset[10];
        unsigned int valeur = 0;
        *Offset = '\0';
        /*Copie jusqu'a la parenthese*/
        while('('!=*(input+k) && '\0'!=*(input+k))/*detection*/
        {
                Offset[k]=*(input+k);/*copie*/
                Offset[k+1]='\0';
                k++;
        }
        /*on rempli la valeur de sortie (overwrite si mnemonique)*/
        valeur = atoi(Offset);
        return(valeur);
}

unsigned int ExtractBase(char *input)
{
        int k=0, off;
        char Base[10];
        *Base='\0';
        unsigned int valeur = 0;
        /*Copie a partir de la parenthese*/
        while('('!=*(input+k) && '\0'!=*(input+k))/*detection*/
        {
                k++;
        }
        k++;
        off = k;
        while(')'!=*(input+k) && '\0'!=*(input+k))
        {
                Base[k-off]=*(input+k);/*copie*/
                k++;
        }
        /*on rempli la valeur de sortie*/
        valeur = OpToNum(Base);
        return(valeur);
}










