#include "graph.h"

void initInterface(){

    initscr();

    regs = subwin(stdscr, LINES , COLS/3, 0, 0);        // Créé une fenêtre de 'LINES / 2' lignes et de COLS colonnes en 0, 0
    PC = subwin(stdscr, 3, COLS/3, 0, COLS/3);        // Créé la même fenêtre que ci-dessus sauf que les coordonnées changent
    IR = subwin(stdscr, 3, COLS/3, 3, COLS/3);
	HI = subwin(stdscr, 3, COLS/3, 6, COLS/3);
	LO = subwin(stdscr, 3, COLS/3, 9, COLS/3);
	MEM = subwin(stdscr, LINES, COLS/3, 0, 2*COLS/3);
	CMD = subwin(stdscr, LINES-12/*2*LINES/3*/, (COLS/3), 12/*(LINES/3)*/, COLS/3);
	
	start_color();
	init_pair(REG_COLOR, COLOR_BLUE, COLOR_WHITE);
	init_pair(PC_COLOR, COLOR_WHITE, COLOR_RED);
	init_pair(IR_COLOR, COLOR_RED, COLOR_GREEN);
	init_pair(HL_COLOR, COLOR_WHITE, COLOR_BLUE);
	init_pair(MEM_COLOR, COLOR_RED, COLOR_WHITE);
	//wintest = subwin(stdscr, 12, COLS/2, 12, (COLS/4)+1);

}

void showInterface(){

	wbkgd(regs, COLOR_PAIR(REG_COLOR));
	wbkgd(PC, COLOR_PAIR(PC_COLOR));
	wbkgd(IR, COLOR_PAIR(IR_COLOR));
	wbkgd(HI, COLOR_PAIR(HL_COLOR));
	wbkgd(LO, COLOR_PAIR(HL_COLOR));
	wbkgd(MEM, COLOR_PAIR(MEM_COLOR));

    wattron(regs, COLOR_PAIR(REG_COLOR));
	box(regs, ACS_VLINE, ACS_HLINE);
	mvwprintw(regs, 0, 1, "registers");
	wattroff(regs, COLOR_PAIR(REG_COLOR));
	wattron(PC, COLOR_PAIR(PC_COLOR));
    box(PC, ACS_VLINE, ACS_HLINE);
	mvwprintw(PC, 0, 1, "PC");
	wattroff(PC, COLOR_PAIR(PC_COLOR));
	wattron(IR, COLOR_PAIR(IR_COLOR));
	box(IR, ACS_VLINE, ACS_HLINE);
	mvwprintw(IR, 0, 1, "IR");
	wattroff(IR, COLOR_PAIR(IR_COLOR));
	wattron(HI, COLOR_PAIR(HL_COLOR));
    box(HI, ACS_VLINE, ACS_HLINE);
	mvwprintw(HI, 0, 1, "HI");
	wattroff(HI, COLOR_PAIR(HL_COLOR));
	wattron(LO, COLOR_PAIR(HL_COLOR));
	box(LO, ACS_VLINE, ACS_HLINE);
	mvwprintw(LO, 0, 1, "LO");
	wattroff(LO, COLOR_PAIR(HL_COLOR));
	wattron(MEM, COLOR_PAIR(MEM_COLOR));
	box(MEM, ACS_VLINE, ACS_HLINE);
	mvwprintw(MEM, 0, 1, "memory");
	wattroff(MEM, COLOR_PAIR(MEM_COLOR));
	box(CMD, ACS_VLINE, ACS_HLINE);
    mvwprintw(CMD, 0, 1, "commandes");
	wnoutrefresh(regs);
	wnoutrefresh(PC);
	wnoutrefresh(IR);
	wnoutrefresh(HI);
	wnoutrefresh(LO);
	wnoutrefresh(CMD);
	wnoutrefresh(MEM);
	doupdate();

}

void showRegs(core *proc){

	wattron(regs, COLOR_PAIR(REG_COLOR));
	for(int i=0; i<32; i++){

		mvwprintw(regs, i+1, 1, "r%02d : 0x%08x", i, proc->registers[i]);

	}
	wattroff(regs, COLOR_PAIR(REG_COLOR));
	wattron(PC, COLOR_PAIR(PC_COLOR));
	mvwprintw(PC, 1, 1, ": 0x%08x",proc->PC);
	wattroff(PC, COLOR_PAIR(PC_COLOR));
	wattron(IR, COLOR_PAIR(IR_COLOR));
	mvwprintw(IR, 1, 1, ": 0x%08x",proc->IR);
	wattroff(IR, COLOR_PAIR(IR_COLOR));
	wattron(HI, COLOR_PAIR(HL_COLOR));
	mvwprintw(HI, 1, 1, ": 0x%08x",proc->HI);
	wattroff(HI, COLOR_PAIR(HL_COLOR));
	wattron(LO, COLOR_PAIR(HL_COLOR));
	mvwprintw(LO, 1, 1, ": 0x%08x",proc->LO);
	wattroff(LO, COLOR_PAIR(HL_COLOR));
	wnoutrefresh(regs);
	wnoutrefresh(PC);
	wnoutrefresh(IR);
	wnoutrefresh(HI);
	wnoutrefresh(LO);
	doupdate();

}

int handleCMD(int *lns){

	//char comm[20],param[40];

	/*mvwprintw(CMD, ++(*lns), 1, ">");
	mvwscanw(CMD, *lns,2,"%s %s",comm,param);
	if(strcmp(comm, "load")==0){

	}else if(strcmp(comm, "start")==0){

	}else if(strcmp(comm, "setout")==0){

	}else if(strcmp(comm, "exit")==0){

	}*/

}

void showMemory(char *memory, unsigned int startaddr){

	box(MEM, ACS_VLINE, ACS_HLINE);
	mvwprintw(MEM, 0, 1, "memory");
	int temp;
	
	for(int i=0, j=0; j<(LINES-2); i+=4, j++){

		/*temp = memory[startaddr + i + 3]&0xff;//memory[startaddr + i + 3];
      	temp += (memory[startaddr + i + 2]<<8)&0xff00;
      	temp += (memory[startaddr + i + 1]<<16)&0xff0000;
      	temp += (memory[startaddr + i]<<24)&0xff000000;*/

		mvwprintw(MEM, j+1, 1, "%04x : 0x%02x%02x%02x%02x", startaddr+i, *(memory+startaddr+i)&0xff, *(memory+startaddr+i+1)&0xff, *(memory+startaddr+i+2)&0xff, *(memory+startaddr+i+3)&0xff);
		if(COLS/3-2 > 26){
			i+=4;
			mvwprintw(MEM, j+1, 19, "%02x%02x%02x%02x", *(memory+startaddr+i)&0xff, *(memory+startaddr+i+1)&0xff, *(memory+startaddr+i+2)&0xff, *(memory+startaddr+i+3)&0xff);
		}if(COLS/3-2 > 35){
			i+=4;
			mvwprintw(MEM, j+1, 28, "%02x%02x%02x%02x", *(memory+startaddr+i)&0xff, *(memory+startaddr+i+1)&0xff, *(memory+startaddr+i+2)&0xff, *(memory+startaddr+i+3)&0xff);
		}if(COLS/3-2 > 44){
			i+=4;
			mvwprintw(MEM, j+1, 37, "%02x%02x%02x%02x", *(memory+startaddr+i)&0xff, *(memory+startaddr+i+1)&0xff, *(memory+startaddr+i+2)&0xff, *(memory+startaddr+i+3)&0xff);
		}if(COLS/3-2 > 53){
			i+=4;
			mvwprintw(MEM, j+1, 46, "%02x%02x%02x%02x", *(memory+startaddr+i)&0xff, *(memory+startaddr+i+1)&0xff, *(memory+startaddr+i+2)&0xff, *(memory+startaddr+i+3)&0xff);
		}

	}
	wrefresh(MEM);

}

void AhA(int oulala){

	if(oulala == -1){
		/*printf("xxxxxxxxxnxxnnnnnnnnnnnnnnnnnnnnnnnnnxxxxxxxxxxxxxxxMMMMMMMxxxxxnnnnnnxxnWMWMWWWWMWAWWWMMMMMMMxMMxxxxxxMMMMMMMMMMMMMMMMWWWWWWWWWWMMMMMMWWWWWMWWWWWWWWW\nxxxxxxxxxxxnnnnnnnnnnnnnnnnnnnnnnnnnnnnxxxxxxxxxxxxxxMMMMMxxxxxnnnnnnnxnxxWWWMWMWMAWWWWMMMMMMxxxxxxxxxxxxMMMMMMMMMMMMMMWMMWWMMMMMMMMMMMWWWWWMWWWWWWWWW\nxxxxxxxxxnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnxxxxxxxxxxxxxxMMMMxxxxxxnnnnnnnxMMMMWWWWWWWWMMMMWMMxxxxxxxxxxxxxxxxMMMMMMMMMMMMMWMMMWMMMMMMMMMMMMMMWMMWWMWWWWWW\nxxxxxxxxxnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnxxxxxxxxxxxxMMMMMxxxxxnnnnnnnxxWAMnnnxnnxMWWAAWMMMxxxxxxxxxxxxxxxxxMMMMMMMMMMMMMMMMWMMMMMMMMMMMMMMWMMMMMMMMMMM\nxxxxxxxxnnnnnnnnnnnnnnnnnnzzzzznnnnnnnnnxxxxxxxxxxMMMMMMxxxxxxnnnnnnnxxMM;:ii****++zMAWMMMMMMxxnnnnnnnnnxxxxxxMMMMMMMMMMMMMWMMMMMMMMMMMMMMMMMMMMMMMMMM\nxxxxxxxnnnnnnnnnnnnnnnnzzzzzzzzzznnnnnnnnxxxxxxxxxMMMMMMxxxxxnnnnnnnxMMM:,i****++++++xAMMMxn+nxxxnnnzznnnnnxxxxMMMMMMMMMMMMWMMMMMMMMMMMMMMMMMMMMMMMMMM\nxxxxxxxnnnnnnnnnnnnnzzzzzzzzzzzzznnnnnnnnxxxxMxxxxMxMMMxxxxxnnnnnnznxMW*.:i****+++**++xWMMxn.`.,;i+znnxxnnnnxxxMMMMMMMMMMMMWMMMMMMMMMMMMMMMMMMMMMMMMMM\nxxxxxxnnnnnnnnnnzzzzzzzzzzzzzzzzznnnnnnnxxxxxMMxxMxxMMMxxxxxnnnnnzznMWz.,:i****+++++**#WMxxz,....``..,:i+nxnnxxxMMMMMMMMMMMWMMMMMMMMMMMMMMMMMMMMMMMMMM\nxxxxxxnnnnnnnnnnzzzzzzzzzzzzzzzzzznnnnnnnxxxMMMMMMMxMMxxxxxnnnnnnzzzWni.:;i******+++***xWxxn+..........``,#xnnxxMMMMMMMMMMMWMMMMMMMMMMMMMMMMMMMMMMMMMM\nxxxxxnnnnnnnnnnzzzzzzzzzzzzzzzzzzzzznnnnxxxxxMMMMxMxxMxxxxxnnnnnzzzzWn;.,;i***********ixWMxn#,,,........`:zxnnxxMMMMMMMMMMMWMMMMMMMMMMMMMMMMMMMMMMMMMM\nxxxxxnnnnxnnnzzzzzzzzzzzzzzzzzzzzzzznnnnnxxxxxMMxxMxxxxxxxxnnnnzzzzzMx:.:;ii***+####*iixWMxxxi,:........`:zxnnnxMMMMMMMMMMMWMMMMMMMMMMMMMMMMMMMMMMMMMM\nxxxxxnnnnnnnnzzzzzzzzzzzzzzzzzzzzzzznnnnnxxxxxxxxxxxxxxxxxxnnnnnzzzzWM:inMM#+*#xWWWWx*izWWMxMi,:.`.......:znznnxxMMMMMMMMMMMMMMMMMMMMMMxxxxxxxxxxxxMMM\nxxxxnnnnnnnnzzzzzzzzzzzzzzzzzzzzzzzznnnnnxxxxxxxxxxxxxxxxxxnnnnnzzzzAM:*zzxxn#zznnz##z*+WMMxM#*i,....``.`:znznnxxMMMMMMMMMMMMMMMMMMMMxxxxxxxxxxxxxxxxx\nxxxxnnnnnnnnzzzzzzzzzzzzzzzzzzzzzzznnnnnnxxxxxxxxxxxxxxxxxnnnnnzzzz#M,.i+MWM:#ixWxWxz*i+WWWMxxnMMMxn+i;,.:znznnxxMMMMMMMMMMMMMMMMMMMxxxxxxxxxxxxxxxxxx\nxxxxnnnnnnnzzzzzzzzzzz#zn#zzzzzzzzzznnnnnxxxxxxxxxxxxxxxxxnnnnnzzzzz+,.+nxWx,xzzWMxzzn*zWWAWWxzn####zxMMMMMzznnnxMMMMMMMMMMMMMMMMMxxxxxxxxxxxxxxxxxxxx\nxxxxnnnnnnnzzzzzzzzzz####zzzzzzzzzzznnnnnnxxxxxxxxxxxxxxxxnnnnzzzzzz#:;i*nzi:++*MMn#++*iMAWMWz:ii;i**i+##nxzznnxxxMMMMMMMMMMMMMMxxxxxxxxxxxxxxxxxxxxxx\nxxxxnnnnnnzzzzzzzzzz####zzzzzzzzzzzznnnnnnnnxxxxxxxxxxxxxnnnnnzzzzzzx;,:;+i:i++*#xn+***ix+i#WW**+*+#####*#nzznnnxxMMMMMMMMMMMMMMxxxxxxxxxxxxxxxxxxxxxx\nxxxnnnnnnnnzzzzzzz#######zzz#zzzzzzzznnnnnnnxxxxxxxxxxxxxnnnnnzzzzz#x*.:;+:;;++#+zn+**ii+*+zMW+##+***iiii#nzznnnxxMMMMMMMMMMMMMMMxxxxxxxxxxxxxxxxxxxxx\nxxxxnnnnnnnzzzzzzzz##zzz#+*iizzzzzzznnnnnnnxxxxxxxxxxxxxxnnnnnzzzzzzx+,;i**ii+iixn#+**i**nx+nW***ii*++#n#xn#zznnxxMMMMMMMMMMMMMMMxxxxxxxxxxxnnnnxxxxxx\nnnnxxnnnnnnzzzzzzzzz##+***+znzzzzzznnnnnnnnzznnnnnnxxxxxnnnnzzzzzzz#xi;,::**zzxz##***ii**#x+WW*##zzzz###*znzzznnxxxMMMMMMMMMMMMMMxxxxxxxxnnnnnnnnxxxxx\nMxzxnnnzzzzzzzzzzzz#+****#zz+zzzzzznnnnnnxnzxnxnnnnxxMWWWWWWMzzzzzznx*i,::i*zxxn#+***i**+#+zAMi+***++**+*znzzznnnxxMMMMMMMMMMMMMxxxxxxxnnnnnnnnnnnnxxx\nM#zx####n#zznzzzzzzz+**++ii;iznzzzznnnnnnnnzMnxnnnnxxMxnnxxxMzzzzzzxxz.,::i**+++#z*iii*++*#WAW+*+*++##zxzxn#zznnnxxMMMMMMMMMMMMxxxxxxnnnnnnnnnnnnnnnnn\nMznx####n##znzzzzz#++++#nxxxx**zzzznnnnnnnnzMnnnnnnxMWnzn#+zMnzzzzx++Wi.,:+nxxnnz#*ii**#MxAAAAnzz#######+zn#zzzznxnxxMMMMMMMMMMxxxxxxnnnnnnnnnnnnnnnnn\nzznMnz##xz#nzzzzzz#+++###znxx++zzzznnnnnnnnzxnnnnnnxMWxznzznxnzzzzM+*MW,,:#i*+#++#*ii**+WAAAWAx+*++#+++##nn#zzzznxxxnxxMMMMMMMMxxxxxnnnnnnnnnnnnnnnWAA\nnzxMzz+#nMxxzzzzzzz*#zzzzzzzzz+zzzzznnnnnnnzMMxnnnnxMWxzznzxxnzzzzW#+nA;.:i*zxz++++ii**nAAWWAAWxz#zzznnnzxn#zznnnnzxxMxMMMMMMxxxxxxxnnnnnnnnnnnnnnnWAA\nnzxM#+##+++*znzzz##+#zz#znnnnz#nzzzznnnnnnnznnnnnnzxxWxzz#zzxnzzzzM++#A#.:ii*++++++****nAAAWWWx+*+#####z#nn#zzxz#nMMnxnMMMMWMxxxxxxxnnnnnnnnnnnnnnnWAA\nWMnMz##+++++znzzz+*****i+nMMMMM#zzzzznnnnnnznnnnnnzxMWxznzznxnzzzzx+inWW:.:;*+++#++***znAAAAWAWxzzzzzzzz#zn#zzxn#xxxxxxMMMMMMMxxxxxxnnnnnznnMn#zzxnWAA\nxnnMn#znnMMn#zzxMxxxxnxxxMMWMMMnzzzzzznnnnnznnnnnnzxxWxzz#Mnxzzzznx+*WMWM;,;+##########nAAAWWAAn+zzzznzz#nx#zznn+znxnnxMMMMWMMxxxxxxnnnz####nMnxxMMAAA\nnnnMx####nzn+MnAAAAWWWAWWWWWWWWWz#zzzznnnnnznnnnnnznxWMnMnnnxzzzzx##WMxWMW;*zzznnnnz#++xAAAWAAAW#zzn#+++*zx#zznz#znnnxxMMMMWMMMxxxxxxnnnnnnxWWMWWWWWWW\nxWxxx+##znzx+x#WAWWWWWWWWMWMWWMWz#zzzznnnnnznnnnzzznxWMMMMMMWzzzzM+zWMMMMW;;#xMxnnz#+*+MAAWAWAAWzzzzzzzz#n++++*+#nxnznxMMMAAAWAAAAAAAAWAAAAAWAAWAAAAAA\nxAMxMzx#nxWW+xzAAAWWWWWWM+*+#AWWzzzzznnnnnnzznnnnzznxWMMMMMxWzzzzx#+MWAWzM*.;+zzzz##++#WAAAAMAWM####+****nnzzzz#zMznMMxMMMAAz*nAAAWWni*AAWW#i#AAWWM**M\nnMMxWMWxnxMx###WAAWWWWWWMn#znWWMz#zzzznnnnnzznnnzzznxWMMMMMxWzzzzM#+xMWxMM#.,i#####++*#AAAAAAAMMMnnnnxxnzn+i##zzxxznnnxMMMAAWMWAAAWWWMMAWWWMxWAWWWWMMA\nxxnxWMMMMWWxn#zWAAWWWWWMMWWWMMMMz#zzzznnnnnzznnnnnznxWMMMMMxWzzzzWn*xWMxnxn.,*#+++++*+nAAWWWWAWWnzzzzzzz#xn#zzz#xxxnzxxxMWAAWWAWAAWWWWWWWWWWWWWWWWWMWA\nMMMxWWWWWWWMxnnAAAWWWMWMMxnxxMMMz#zzzznnnnnzzzzzznnnxMWMMMMMMzzzzMMMWMMWxxM.,*#++++**+MAAWWAAAWWnzzzznnzzxn#####xxxnznxxMWAAWWWWWAWWWWWWWWWWWWWWWWAAAA\nWWMxMWMMxxxxMWAAAAWWWMMMMMMMMMMMz#zzzznnnnnMMWWMMMxnnxxxxxxnnzzzzzznnWMMWWA:,i#+++***+xAAAAAWAWMxzzzznnzz#++###nxz#zxxxxMWAAWWWWWAWWWWWWWWWWWWWWWWAAAA\nMMxxxxxxxnnnnnxAAAWWMMMMMMMMMMMMz#zzzznnnnnnnxnnnnnnnnnnnnnnzzzzzzzz#zMMWWA+.i++*+***+zMMWAAAAWWnzzzzznzzn#+##nzxzz+#nznMWAWAAWWWAWWWAWWWWWWAAWAAWAAAA\nxxxxxxxnnnnnnnxAAAWWMMMMMMWMMMMMz#zzznnnnnnnnnnnnnnnnnnnnnnnnzzzzzzz##zMxMWx.;+*++***+*#xMAAAWMMx##znxMxn##zz+z#nzznMn#xMWAAxnWWWAWWWMWWWWWWWWWWWWWAAW\nxxxxxxnnnnnnnnxAAAWWMMMMMMWMMMMMz#zzzznnnnnnnnnnnnnnnnnnnnnnzzzzzzzznnn###xA;:*++***++#n#WAAAWWMMn#zi;;::*#+##M+nnM#xnnxMWAAz+MAAAWWzi#AAWW*inAAAAx#zA\nxxxxxnnnnnnnnnxAAAWWMMMMMMMMMMMMn#zzzzznnnnnnnnnnnnnnnnnnznnznnnnnxxxxxxxxWA*,*+****+#n##AAAWMMxxnzz:..ii;i*;i**++#zxnnxMWAAMMAWAAWWWWWAWWAMxWAAAAMznA\nxxxxnnnnnnnnnnxAAAWWWMMMMMMMMMMMn#zzzzznnnxxxxMMMMWWWWWWWWWWWWMMxxnzzznznWAM+,***i**#z#iWAAWxxMnnz#nx#,;iizW#i#AWWnznxnxMWAAAAAWAAWWWAAAAWWWWAWAAAWMWA\nxxxxnnnnnnnnnnxAAAWWWWWWMMMMMMMMn#zzznnMAWWWWMxxxxnnnnzz###+#*#+**+#nz##xWA#+,**i;*+zz*zAAAWMxnxxxWWMMMz;inM#*#xzxnzxnnnxWAAAAWWAAWWAAAAAAAAWWMAAAAAAA\nxxxxnnnnnnnnnxxWAAWWWMMMMMMWMMMMn#zzznnxn+++##++zzxxnz#zzzz#+i*Mx+#zz#+nWWWi:,*i;:;+##iAWAAMxxnnAMMMMMMMxi#xz+#n#nnnWxnxMWAAAAAWAAWWMz######+#####xAAA\nxxxxxnnnnnnnnxxWAAWWWWWWWn++#WWMn#zzznnxx+#+#+zz####++znz##n+i+AWz+#z+nWWA#,,,+i,,:**:nAAAWnMxnWWWMxnxxxWWnxn##zz#nWMMxnMWAAAAWWAAWWnznnnnnnMMnxWWMAAA\nxxxnnnnnnnnnnxxWAAWWWWWWWn;i+WWMx#zzzznxx+#+#####z#n++z#+##ni*zA+z+##nWWWW:;,,+;,::i;;WWAAMxMxMWxxxxxnxnnxWnz##nxnzxnxzxMWAA#+WAAAWWnznnnnnxAAnWWWMznx\nzxnnzzznnnnnnxxWAAWWWWWWWWWWWWMMx#zzzznxx+z#z#z###+n#+zx#zz+#+Mn*zz#MWWWWx.i:,;:::;;i*AMAAnWxMMxnxxMxxxnnnMMz#znnznxnnnzMWAAMxAAAAAAnnxxnnnxAMz#AMM+##\nxxnnzzzxnnnnnxxWAAWWWWWWWxnnnxMMx#zzznnxM+zzn##+znzMAx#n+#+znnn+#x#nAWWWW#.n*,::::i*;MAWAWMWMMnxxxxxxzxnznnWn#znnnxMznnxMWAAWWWxznxMxMMWxnnxAWM#WAM#n#\nxznznnzxnnnnxxxWAAAAWWWWMWWMWMMMx#zzzznxM+#+###+zxzzz#zx#+#zzn*++xzMzzAWW;.M#::::*+;zAAAAMWWMnxxxnxnxnxzznnMM#zn##nxnMxxMWAAAAMn**++++#xAWxxAWn#WMMnxz\nxnnznnnnnnnnnnxWAAAAWWWWMMMMMMMMxzzzznnxW+#zn##z#znxnnz+*nzzn#;#znzzWWWWW,,Mni:;++;:MAAAAWAWxnxxxnzxMMxnzznMAMxxxWMnzAWxMWAAAAMnx++###zxMWMxAWnxWWMAAA\nxzxznnzxxzzzzznW#AAWWWWMMMMMMMMWxzzzznnxW++zzMWz#nznz#z##xxxM+##zn*iWAWWM,.:*i*+*;;:zAAAAAAMnnnnxxzxMMxMxzzxAAWWWWxnxWAxMAAAAAxnxx++##zxWWMxAWWWWAMWWW\nxnxxxnzzz##zznxWAAAWWWWMMMMMMMMMxzzzznnxW++nn+#zn#z#####M*#+z*n#nxi*MMWWx..,i*+*i:i:#AAAAAWMxzznMxnnxMxxxxnxWAWAW+i*MMMxnxMWMMxxxxx+##zMWWMxAMMAMAMWWW\nMnMWWWxzznznnxxWAAAWWWWMMMMMWMMMxzzzznnxW++z#*+#++nM##xzxnzn#i+xnM*#WxMWn...:i*+:;;,nAAAAMWMxz#MMxxxnMxMxxnxxAWAWz#znn;i**+z#nMxxxMn+#zMWWMxMMMMMMxMWW\nMnxxxxMnnxxMnnnWAAWWWWWMMMMMMMMMxzzznnnxW#zzzzn++##xxnz####++**MzM+zAxMW#.,.;**:;;:;xAAAWMMnz#xxxxxxnMWnnxxxxAWAWM#zxn#+##+znnMMxMxMn+zMxxxMxxxnnnznnM\nMnnnnzMznWAnnnnWAWWWWWMMMMMMMMMMxzzzznnxM####znnz##zzz#z+z##*z+Mzx+nAzMW#,,.;*:;i:,#MAAAMMWxzxMxxMMxxMzznxxxxAWAWWzi*****#z##+#nzxMMMz#MMMnnnnMMWWAAAM\nxnnnnnMnnxnnxnnWAWWWWWMMMMMMMMxxxzzznnnnMz###zznxnnx#zz#zzz+*#znnx#MW+WW+,;.i::*::.xAWWWxMWxxMMMMxxMxn#+xMnMxAWAWMxxWMxnnn+++**i;;;zxxMAWMxznnxMxxnnnz\nnnnnnnxnnnxAWnnMWWWWMMxMMMnnzWMMxznnnnnnxz+##nz#z#+#zzxnzzzzz*xzxn#WnzMM*,;,,:*:::;xWWWWnMWxMMMMnMxxxxzMMnnWxWWAMWn#MMxxxMMMx#zzzz##nMWWWMxnxMMn##zzzz\nMnznn##znxnxnnnWAWWMMMMxMM***MMMxznznnnnxn+#nnxxxnMxz#zzzzzMz+nMnnzM#xxM#i;::z+,+*izWWWMnMMMxWxxMnzMMMMxnnMMMMWWWWnnMMMMWWWWxnnzzzzxMWWWWMMMn++zz#z#zn\nMnznz#z#n#;*##n+;i;;MM#,;;:zMMxx#::,;nnnnn#+i;,:;;::;;,::;xWz::;;:+Mi:;;:::,i*,.:,,;;i*zxxn:;;:;+MMxx;::;;*MWMMM*;;;+WWWWMi;;::nMxi;;iiWn++#MMMWMMnxMA\nMMnzzz#z+iiiii*,:i*:MMi,ii:#xnx#.:;;:#nnnn*,.,;;;;;;,.:;;,#n,,:;;.;,,:;:::;:,,,,::::;;:,iM+.;;;i:xMx+,:;ii,xxMM+.:;i:WWMMx,;i;::+*,;i*:Wz#*+nzzzMWWWWW\nMzzzz#nz***#z+#,:;;;Mx:::;:xxxn:,::::*nzn+.,:::::::::.:::i+,:::,::.,::::::::i.,:::::::::,n;,::::innn,:::::*nzMn,:,:;.#z#z#,:::;:+i,;;;i#;:*#znnnnxWAAA\nxznnznn#xnnnz#*,:::#xz,,,,#nnn+.,,,,,+z##,,,,..,,,,,:.,,,*,,,,.i+.,,,..,,,,:*..,,....,,,,z:,,,,,*nni,,,,,:znzn:,,,,:.+Mx*:,,,,:;xz,:::nz#*i;:,,,,::;i#\nMzxnnAxzxMWMMxi,,:;xni.,,,nnzz,..,,,.+z#;..,.;z#;;#*,.,,;;..,,+#:..,,##+:,i+i..,.;z#*..,.+,..,,,*n+..,,,,iznz+..,,,,,+#zn,.,,,,,#*.,:;MWWWAAWWMnz+*+xW\nMxnnnAxnMWMn++:,,,:;,.,,,,zxn;...:.,,;z+....:+##z#*+,..,,....;n*....,;,..,;i:..,,i+*:....*....,,:z:....,.*nnz,...,.,.zn#*..,,,,,ii.,:iMWMMWWWAAAAAAWWW\nWWxWWWxnMMMMMn..,,,,,,,,,;Mx+...,n.`.,n;`...*###+#++........;nx,...........i.`..........:*`......i.......*Mx;...;*`..iWM*...,,,,,,,,,iWWWWWWWWWWWWMWMW\nMMn###xAAWWMx*.,,,,,,,.,,;Mn,...;+....i...,,#xz#+#+i.....,;:xnz..........,,i:.........,:#,..,...,.......,#M+...,zi..,,Mxi..,;:..,.,,:*WWWWWWWWWAAWWMMA\n#+#+#zzxWWWMM*xMMnzzznnn*#zi#xxni*zxn*,:xxninxxxz#+;zxnznnn:#z+innn#######ii#xnnnnnnni,#z;nn#:nn+*nni#nniz#:xxxz+ixxx*znixxx++nxz+xM#zMMMWWWWAAAAWAWAA\n++#zxMMxWWMxx*zzz+zi:#zz*z+*zzzzzz###++i###+xxMMMxn.##z;###*+z#*##+i+++*ii*;##z**;*##;;z+*+#*,#####+;###iz:*zzzzzz##z+zz*zzz+#+zzzzn#xxMMWWAAAAAAAAAAA\nxMWMMMMWWMMMz,;iizxx:;;;;z.;;;;;;::::;.,::;:*i****+,:::#,::;;#+,,::.i;i;::;.::;*#+,:;:,ni,,:;;::;;:,,:::,;,;::::::,:;;##,:;;zz:i;;ii#MWWWAAAAAAAAAAAAA\nMMMxxMWWWMMM+,,:;xxn,,,,*;,,,:zi;i:..,....,,,,.,,,;..,,z:..,,;n.`.,,...,,:;..,.znn...,.z:...i;`....i.`....,,.i#+*:.,,,++..,:xx,.,:::+AAAAAAAAAAAAAAAAA\nWMxMxxMWWMMMi.,,*Mx#..,,,.,,.#xnzz;...,*.`.......;i`..iz+....;M*.........*i...:zzM;....*:`..+i`...**`.......,zxzz+....ii`.,iMM*.,,::zAAAAAAAAAAAAAAAAA\nMMnxMWMWMMxx:.,.zMx+.....,..innz+*i`...z*,.......*;``.+z#i`...#xi........#;`..i+zM#....#,`..;*``.,n;``.*,`..*Mxzzz:...;;`..#Mxz,.,,*WWWWAAAAAAAAAAAAAA\nWMMAWWMMMMMx####xxnz###i,;##+iii****+,+xMn#######z+##++*++#zz#zxx#*++##+#+i+++*i*Mn*++++++###++##zz+###z####nxxnnz++i;###zznxxxz#z:#WWWAAAAAAAAAAAWAWW\nWWMxAAWWWMxzzzn#xxxzz***++#*ii+n***iiixMMMxxxxxxxnn##z++#+#nxMM+:*nnnnz+++:;:i:i*W*;++#+***+#####zzzxnzznnznnxxxxxxxxxxxxxxxxxxxxxMMWWWAAAAAAAAAAAWMxn\nWWM#MAWWWWWWWMMMMM#+z#zzzzz#;i*ni;*+#*inWMxxxx+*++*++##zz###nM#.:;#MMMi:*+:i:,,izz;#+**+#M#+#zzz#znxnxMnnxnxxMMMxxxxxMxxxMxxxxMMMMMMWWAAAAAAAAAAWWMnnz\nAAx#nWAAWWWWWWWWWMn#zxn#####znxnx;+#znx#nWMMWnx#+*++xMnMMMxzzx;,::;;+nMn,:;ii;,zWiznnzMxnWM##zznnnzznxWxnMMMMMMWMMMMMMMMMWMMMMMMMMWWWAAAAAAAAWAAAWWWWM\n+*+zznWAAWWWWWWWWWWWMxzzz#znMAWWW*+zzzWzxWWM+MWMn++nAzxz#xxWMx,:ii;:;i*z#+zz*;.#xixz++znMWxW#znxnnnnzzMWWnnMMMxMMWMMMMMMWWMMMMWWMWWWAAAAAAAAAWAAAWAAAA\n:;###zxWAWWWWWWWWWWWWWWWWWWAWWWWMz*znzMnAWz,,+xzWx+nWxnn+*xMnMi*i;;::;;i*+##**xMxxxzMnMWxWMxzxxnnnnxnzznMAMnMMMxMWWWMWWWWWWWMWWWWWWWAAWAAAAAWMAAAAAAAA\nxMn*+#zxAAWWWWWWWWWWWWWWWWWWWWAAnx+xxzzMWWWx+:inWAxxMxxx##ni*#+;iii;;:;;;;iiinMzxnMzMMWAMxAzxMxxxxxxxzzznnMMMMMxxAWWWWWWWWWWMWWWWWWWWAWAAAAAWxAAAAAAAA\nnAAz*+#zWAAAAAWWWWWWWWWWWWWWWMWWxn+nMWxAWWWWWWn*nAAWMnxnnxii**+;iiiiii;;;;;;i*nnxnxMnMMWAxMAAMxxMMMxxxzzzzzMMMMnxWWWWWWWWWWWWWWWWWWAAAAAAAAAAxAAAAAAAA\nnxAW+*+#AAAAWWAAAWWWWWWWWWWWWWMWWzMWWnMWWWWWWWMMWWAWMxMxMz:;i*++#*iiiii;i;;;**nnnxzWnWWWAnxxAAxxxMMMAMx#z#+nMxMMxMWWWWWWWWWWWWWWWWWAAAAAAAAAAnAAAAAAAA\nxzMAM**zAAAAxxxxMMWAAAWWWWWWWWWWWxAWWWWWWWMMWAAWxWWWWxz*A*:;ii*+#xz***iiiiiixxzzxxzWxMWWWnWMMAAnnxMxAWAn+#zzxnMxnxWWWWWWWWWWWMWWWWWAAAAAAAAAAnAAAAAAAA\nMzzAAz*MAxAWxxxxxxxxxMMWAAWWWMMWWMAAWAWWWWWWnMWWWWW+;Wx#Wi;iiiiii*+#+*******WM+zMMMMMxWWMxMMxMAAMMMxxWAWnxMWWWWWWMWWWWWAWWWWWWWWWWWAAAAAAAAAAnAAAAAAAA\nn+*xAAMAWMWMxxxxxxxxxxxxxxMMWWWWWWWWAAWWWWMWWxWAWzWWzxnWW***+**iiii*++*****+WMnxMMWWWnxxWxnx#xMAWMMxMxWAMWAAAAAAMMWWWWWAAWWWWWWWWWWAAAAAAAAAAzAAAAAAAA\n#z++MAAAAAWxxxxMxxxxxxxxxxxxxAAAAAWWWAWWWMWAAWAWWWWWWWxWAz**+##++*iiii*****#WMnxxWWMAxMMMxxx##xWAnWWMxMAWMWWAWWWzWWWWWWAWWWWWWWWWWWAAAAAAAAAAnAAAAAAAA\nxMMWWxAAAAMxxxMMxxxxxxxxxxxxWAAAAAAAAAWWWWWWWWWWWWWWWWWMAWz**++###+***i*i**#WWnxMWWWAMMWMWAAMn+zWMWWxxxWAWMMWWWMzWWWWWWAWWWWWWWWWWWAAAAAAAAAAnWAAAAAAA\n");*/

		printf("                                               ixz*i:`                                                                                                \n                                              `M@@;`:*,                                                                                               \n                                              *x@#:*.`*.                                                                                              \n                                             .*`.``.+`.*                                                                                              \n                                             ,;`   `,*`i.                                                                                             \n                                             ,; ` ```*,.*                                                                                             \n                                             .i```````+`*`                                                                                            \n                                              *```````:i,;                                                                                            \n                                              i.`;*````+`+`                                                                                           \n                                              `*`;n``` :i,;                                                                                           \n                                               ;,.+.;.``+.*`                                                                                          \n                                                *.*n,```,+`*                                                                                          \n                                                :;`*.``,`i;::                          `.`                                                            \n                                                 *.,*`:i``*,*`       .:.     .;i;,`  .i+*+.                                                           \n                                           `.`   .+**:*,` `+:#:.` `,**i**i:;*+i;;i++*+i;ii+.                                                          \n                                          ,*i***iiz,*z;````i+z;i***i;;;;;iii*#z#+;;;;;:;*;ii                                                          \n                                         `*;;i*****#```````;@M:;;+#z+;;;;;;;zxxnx*;;;ii*i;;*                                                          \n                                         :i:+nnnxnnxi``````n@@+:*xnnn;;;;;;;*#zzzi;;iii;;;;*                                                          \n                                     `,:i*;;*nnnxxxnz*.``:n@@xx:;i**++++i;;;;;;;;;;;*;;;;;ii                                                          \n                                    `**ii;;;;;ii*+*i;ixxW@@@x*x;;iznnnxnnn+;;;;;;;;;*;;;;;i:                                                          \n                                    .*;;;;;;;i+++*;;;;*@@@Wni*x;;#xnnnnnnnn;;;;;;;;ii;;;;;*.                                                          \n                                 `,;i;;;;;;;;+xxxx*;;;zz#+*iinn;;i#nnnnnnz+;;;;;;**i;;;;;;i:                                                          \n                                ,**i;;;;;;;;;;*+##i;*n#iiiiinn+;;;;;;iii;;;;;;;;i;;;;;;;;;;*.                                                         \n                               :+*+*;;;;i+++*;;;;i#nz*iiii;z#ni*i;;;i*ii**;;;i**+;;;;;;+z+;;*.                                                        \n                               *;;;i++++i;:;i+++*n#*iiiiiiz#z+;;i***i;;;;;iiii;;;i;;;;zxnn+:;*                                                        \n                               +::::;;;;::::::;::n*iiiii*n##z:::::;::::;++*;::::;*;;;*xnnnn;:+`                                                       \n                               ;i:;++++*;::::::::#z++*+zn+zz:::::::::;i*###;:::::i;;;zxnnnn;;*                                                        \n                                :i;+##+*;::::::::;zzzzz##n*::::::::;++*i;;i++;:::i;;innnnnn;;*                 ,,                                     \n                                 .*;;;;*;:::::::::;+znnz*:::::::::;+i:i:::;:;*i::i;;ixnxnxz;;*               `#+z.                                    \n                                  .++;;z:::*::::::::::::::::::::::;;::n::;z:::;::i;;;nxnnx+;;*              ,#;:+,                                    \n                                  ,#:::n;:;n:::::::::::::::::::::*;:::z*i*z::::::*;;;+nxxz;;;*`            :#;::#.                                    \n                                 ,#z;;+x###x*::::::::::::::::::::izi##+ii*+#+::::*;;;;*#+;;;;i;           :#;:i:#`                                    \n                                 #:in+,````.*#;:::::::::::::::::::n+.```` ``:#;::ii;;;;;;;;;;;*i    `.`  :+::ni:+                                     \n                                `#:#;````````,z;:::::::::::::::::#:``   ``  `,#::;*i;;;;;;i*;;;*:  :*ii++z::++;:+                                     \n                                 ##;`;i,` `  `:#::::::::::::::::+;``    `.;:``*i::;ii;;;;;zx+;;ii :;`  `.#i+.+::+   ``                                \n                                 ,#,z+*#+`    `#;:::::::::::::::#```  ` iz+#z.,#:::;*;;;;ixx#;;ii.*```   .n, #::* +#zz`                               \n                                 i,#+xz;#:    `*i::::::::::::::;+      .z*x#*+`z:::;;i;;;*xx*;;;*+```    `*.`#::i,+#;#,                               \n                                 *.zn#@i+i    `ii::::::::::::::i*``    ;#z#@*z.#::::;*;;;in+;;;;z,``     `;:`#:;;;iz;#z,                              \n                                 *.##Mniz,    `#;::::::::::::::;+```   ,z*Wn*+i*::::;*;;;;;;;;;;#``      `i:.+:*+*;##i;i                              \n                                 ;:.z#+zi``  `,#::::::::::::::::z``     *z++z,z:::::;*;;;;;;;;;#:``      ,+`:*:###+*z:i;                              \n                                 `+``:;. ` ` .+;:;::::::::::::::**``    `,i;,zi::::::*;;;;;;;*#n``      ,*` ;i:z*iz:z:+,                              \n                                  ,*` `    `.+i:;+:ii::::::::::::*+.`  ```,+z;:::::::*;;;;;;inxz       ;i   i;:nii;*+:#`                              \n                                   ,+,````.i#;::#;:#;:::::::::::::i##+++###i::*::::::i;;;;;;znx+     `#xnnnnM;:z:;;+:ii                               \n                                    .#z##zz*::;zi::z:::::::::::::::ii;;;;:::i+i;::;i:;*i;;;innx#   `;nz#+###x;;+*#:::#`                               \n                                     `#z+i;::izi::i#:::::::::::::::;*++***++*;:ii:;i::;ii;;+nnnx:,iz++#####+x;:iz;::+:                                \n                                       ;+::;##::::#;:::::::::::::::::;;;;;;::::;;;i::::;i;;+xnnxn#*i:;nxx#xMMi:+*::**                                 \n                                       `*+##i::::;#::::::::::::::::::::::::::::::;;:::::;i;*xnnn*;;;;;ii+*+:nz:;;:*M#                                 \n                                      `+z*;:::::;#;:::::::::::::::::::::::::::::::::::::;i;;nxx#;;;;;;;;+i..nMz*+nMxM`                                \n                                      i*::::::::+*:i*:::::::::::::::::::::::::::::::::::;i;;i+*;;;;*#;;;+,..#M@@Wxxxxi                                \n                                      +:::::::;*+::i+::::::::::::::::;::::::::::::::::::;*;;;;;;;;;nx*;;#...iMW@@xxxxn                                \n                                      +::::::;++:::*+::::::::::;*znxxxxn+;:::::::::::::::ii;;;;;;;ixx*;;+...,MM#@xxxxM.                               \n                                      :#;::;*#*::::;i::::::::inWWWMMMMMWW#::::::::::::::::i**;;;;;;nn;;;+;...nM##xxxxx*                               \n                                       ,+##n#::::::::::::::;nWWMWMMMMWWWM+:::::::::::;:::::;;*;;;;;;;;;;;+...+M@@Mxxxx#                               \n                                           *;:::::::::::::+WWMMMMWWMn#*;;:::::::;i*++++*;:::;;i;;;;;;;;;;#...;MW#WxxMxi                               \n                                           *;:::::::::::;nWWMMMWMzi;::::::;::::;++++###++;:::;*;;;;;;;;;i*...,xMWxMn;                                 \n                                           +;::::::::::;xWWWWWn*;::::::;i+z;:::i#+++#++++;::::*;;;;;;;;;i*....zxxM*`                                  \n                                           *;:::::::::;z*MWWzi:::::;*#zz+i::::::*++++++i;:::::ii;;;;;;;;i+....iMn,                                    \n                                           *;:::::::::n*:nz;::::i#z#*;;:::::::::::;;:::::;ii;:;i**ii;;;;;#,...iz`                                     \n                                           ii::::::::+izn*:::i#z+i::::::::::::::::::::::i+##+;:;:::*;;;i;*;.;+i.                                      \n                                           ;+:::::::i:n;:::*nzi:::::::::::::::::::::::::i##++;:::::;**ii*i##i`                                        \n                                           `z:::::::*`z;;*#;.i*i;:::::::::::::::::::::::;iii;:::::::;;;;;i*`                                          \n                                            *i:::::*, .+#i`   `,i*i;:::::::::::::::::::::::::::::::;;i+zi.                                            \n                                            `#:::::+             `zxz+*;;:::::::::::::::::::::;i*+#zxxxM`                                             \n                                             ;*:::i:            `zMxxxxxnz#+**iiii;;iii***iiixxxxxxxxxxn                                              \n                                              +;::+             #Mxxxxxxxxxn``..,,,,,,.``   +xxxxxxxxxx+                                              \n                                              `+;:i            `MMMMMxxxxxMi                xxMMMMxxxxM:                                              \n                                               .#+.             iMM#izMxxxM`                xMM#:zMMxxx`                                              \n                                                `*              `z*:inMxxMi                 ,#M;:xMMxMi                                               \n                                                               `+i:*+znxx*                   `#:in##+;                                                \n                                                              `+;:+i                         ,*:+:                                                    \n                                                              ;i,in.                         *::z`                                                    \n                                                              .#i+*#+.                       #:;#                                                     \n                                                              `iMx+i;+i`                    .+:in:`   ,*,                                             \n                                                             `n@@@@M;.:#:                   `#:#i+z+::@#@;                                            \n                                                             i####@@@n*#@+                   ,##+;:,izW##@;                                           \n                                                             x##WW#######n                     :+z:.`.i###@.                                          \n                                                             x##@@#######n                       .i#*,+####*                                          \n                                                             +###########+                         `+W@####:                                          \n                                                             `M#####xWWx*`                      `,,.i#####z                                           \n                                                              .z@@W+`                          ,M@#@@###@x`                                           \n                                                                ``                             x#@W#####n`                                            \n                                                                                               W#@@#####i                                             \n                                                                                               n######@#`                                             \n                                                                                               `+xWWn+.                                               \n");

		//getchar();

	}

	
    
    

}