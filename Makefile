
INC_DIR = include

all : main.o parser.o processor.o graph.o
	gcc -o MIPSimulator18 -Wall main.o parser.o processor.o graph.o -lncurses

parser.o : $(INC_DIR)/parser.h $(INC_DIR)/parser.c
	gcc -c $(INC_DIR)/parser.c -o parser.o

processor.o : $(INC_DIR)/processor.h $(INC_DIR)/processor.c
	gcc -c $(INC_DIR)/processor.c -o processor.o

graph.o : $(INC_DIR)/graph.h $(INC_DIR)/graph.c
	gcc -c $(INC_DIR)/graph.c -o graph.o

main.o : main.c
	gcc -c main.c -o main.o

clean : 
	rm main.o parser.o processor.o graph.o