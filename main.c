/*
--------------------------------------------------------------------------------------------------------
source:main.c
author:Anton Dumas, Gael Dezanneau
projet:projet MIPS
organization:INP ESISAR
--------------------------------------------------------------------------------------------------------
*/

#include <stdio.h>
#include "include/processor.h"
#include "include/parser.h"
#include "include/graph.h"
#include <signal.h>

int is_resized=0;

void resize_handle(int dummy);

int main (int argc, char *argv[]){

    char sourcefile[40]={"source.test"},sourcefile2[40];
    char testword[30]="  ADDI $0,$19,34567#test";
    char comm[20],param[40],outfile[40]={"out.hex"};
    unsigned char Memory[800]; // déclaration de la mémoire du simulateur ici 800 octets
    int out=0,nobody,lns=0,lnsmax=20,argtemp=0;
    argtemp=argc;
    //construct teststruct;
    core proc; // déclaration de la structure d'un coeur MIPS proc qui sera utilisée par le simulateur

    //signal(SIGWINCH, resize_handle); 
    
    cbreak();
    
    ProcessorInit(&proc); // initialisation du processeur regs a 0 ...

    //initscr();

    initInterface(); // initialisation de l'interface et de curses

    lnsmax=LINES-14; // définition du nombre de lignes maximum utilisable dans l'invite de commande

    showInterface(); // affichage de l'interface 

    showRegs(&proc); // affichage des registres du processeur dans l'interface

    mvwprintw(CMD, ++lns, 1, "__ASM_MIPS__ V1.0 "); // afficage de l'entéte d'acceuil dans l'invite de commande

    while(out != -1){               // boucle infinie centrale elle ne termine qu'a la fin du programme
        
        comm[0]='\0';param[0]='\0'; // initialisation a 0 des conteneurs de paramétres
        nodelay(stdscr, TRUE);
        if(getch() == KEY_RESIZE ){     // routine en cas de détection d'un redimensionnement de la fenetre 
            is_resized =0;
            //endwin();
            //refresh();
            clear();
            initInterface();
            lnsmax=LINES-14;//2*LINES/3-2;
            showInterface();
            showRegs(&proc);
            showMemory(Memory, 0);
            //doupdate();
            refresh();
            strcpy(comm, "clear");
        }else if(argtemp > 1){          // routine de début lorsque des paramétres de fichiers sont passés lors de l'appel par l'OS
            mvwprintw(CMD, ++lns, 1, "!loaded %s", argv[1]);
            mvwprintw(CMD, ++lns, 1, "!enter start");
            strcpy(comm, "load");
            strcpy(param, argv[1]);
            if(argtemp > 2)strcpy(outfile, argv[2]);
            argtemp=0;
        }else {                         // routine standard de gestion d'affichage de CMD
            mvwprintw(CMD, ++lns, 1, ">");
            mvwscanw(CMD, lns,2,"%s %s",comm,param);
        }
        if(strcmp(comm, "clear")==0 || lns>=lnsmax){    // remise a 0 de l'invite de commande 

            werase(CMD);
            box(CMD, ACS_VLINE, ACS_HLINE);
            mvwprintw(CMD, 0, 1, "commandes");
            wrefresh(CMD);
            lns=0;

        }else if(strcmp(comm, "load")==0){              // chargement d'un fichier dans le simulateur

            if (parse_file(param, outfile, Memory) == -1){ // chargement du fichier
                mvwprintw(CMD, ++lns, 1, "!ERROR!file opening not possible");
            }else{
                mvwprintw(CMD, ++lns, 1, "output in %s",outfile);
                showMemory(Memory, 0);
            }
            wrefresh(CMD);

        }else if(strcmp(comm, "start")==0){             // départ de l'exécution
            ProcessorInit(&proc);       // initialisation du processeur
            int stop=0,temp=0, func=0;
            mvwprintw(CMD, ++lns, 1, "key to pause");
            lns++;
            if(strcmp(param, "step")==0) func=1; // est on en mode step ?
            //nodelay(stdscr, TRUE);
            while(stop ==0 ){
                comm[0]='\0';
                mvwprintw(CMD, lns, 1, "%08x count:%d",(*(Memory+proc.PC)<<24)+(*(Memory+proc.PC+1)<<16)+(*(Memory+proc.PC+2)<<8)+*(Memory+proc.PC+3), temp ); // affichage de l'avancement
                
                stop=ProcessorOperation(&proc, Memory, 0); // execution du cycle processeur
                showRegs(&proc);
                showMemory(Memory, 0);
                //nodelay(CMD, TRUE);
                if(getch() != ERR || func == 1){ // si interruption par l'utilisateur ou mode step, l'attendre 
                    //nodelay(stdscr, FALSE);
                    mvwprintw(CMD, lns+1, 1, "!%02d>",temp);
                    mvwgetstr(CMD, lns+1,5,comm);
                    if(strcmp(comm, "exit")==0) stop= -1;
                }
                temp++;
            }
            switch(stop){           // gestion du retour d'exception 
                case 48: mvwprintw(CMD, ++lns/*lns+1*/, 5, "48:Arithmetic Overflow"); //arithmetic overflow
                break;
                case 60: mvwprintw(CMD, ++lns/*lns+1*/, 5, "60:division by 0"); //float exception (by 0 division)
                break;
                case 32: mvwprintw(CMD, ++lns/*lns+1*/, 5, "32:SYSCALL exit"); //syscall
                break;
                case 24: mvwprintw(CMD, ++lns/*lns+1*/, 5, "24:instruction fetch error"); //data bus error on instruction fetch
                break;
                default:;
                break;
            }

            //if(*comm == '\0') mvwprintw(CMD, ++lns/*lns+1*/, 5, "exited on end",temp);
            //nodelay(stdscr, FALSE);
            //lns++;
        }else if(strcmp(comm, "interactive")==0){           // mode interactif
            //ProcessorInit(&proc);
            proc.PC=0; // mise a 0 de PC
            werase(CMD); // reset de CMD pour corespondre au format du mode
            box(CMD, ACS_VLINE, ACS_HLINE);
            mvwprintw(CMD, 0, 1, "commandes");
            wrefresh(CMD);
            lns=0;
            mvwprintw(CMD, ++lns, 1, "mode intéractif");
            construct instdef; // déclaration de la structure tampon de conversion
            int stop=0,temp;
            while(stop != -1 ){
                mvwprintw(CMD, ++lns, 1, "?>");
                comm[0]='\0';
                mvwgetstr(CMD, lns,3,comm);
                if(strcmp(comm, "exit")==0) stop= -1; // sortie
                else{
                    //mvwprintw(CMD, ++lns, 1, "%s",comm);
                    temp=parse_input(comm, &instdef); // décodage de l'instruction ASM
                    proc.IR=temp;                     // chargement dans IR
                    temp=ProcessorOperation(&proc, Memory, 1); // execution du cycle processeur
                    showRegs(&proc);
                    showMemory(Memory, 0);
                }
                switch(temp){            // gestion du retour d'exception 
                    case 48: mvwprintw(CMD, ++lns/*lns+1*/, 5, "48:Arithmetic Overflow"); //arithmetic overflow
                    break;
                    case 60: mvwprintw(CMD, ++lns/*lns+1*/, 5, "60:division by 0"); //float exception (by 0 division)
                    break;
                    case 32: mvwprintw(CMD, ++lns/*lns+1*/, 5, "32:SYSCALL exit"); //syscall
                    break;
                    case 24: mvwprintw(CMD, ++lns/*lns+1*/, 5, "24:instruction fetch error"); //data bus error on instruction fetch
                    break;
                    default:;
                    break;
                }
            }
        }else if(strcmp(comm, "setout")==0){            // definition du nom du fichier de sortie

            if(*param != '\0')strcpy(outfile,param);

        }else if(strcmp(comm, "help")==0){              // help me please !! (appel de l'aide)

            mvwprintw(CMD, ++lns, 1, "-? load file.asm");
            mvwprintw(CMD, ++lns, 1, "-? setout file.hex");
            mvwprintw(CMD, ++lns, 1, "-? exit");
            mvwprintw(CMD, ++lns, 1, "-? start ?step");
            mvwprintw(CMD, ++lns, 1, "-? interactive");
            mvwprintw(CMD, ++lns, 1, "-? whoisyou?");
            mvwprintw(CMD, ++lns, 1, "-? help");
            wrefresh(CMD);

        }else if(strcmp(comm, "whoisyou?")==0){         // display d'informations sur le projet

            mvwprintw(CMD, ++lns, 1, "PROJET MIPS 3A");
            mvwprintw(CMD, ++lns, 1, "DUMAS ANTON");
            mvwprintw(CMD, ++lns, 1, "DEZANNEAU GAEL");
            mvwprintw(CMD, ++lns, 1, "ESISAR P2021");
            wrefresh(CMD);

        }else if(strcmp(comm, "exit")==0){              // sortie du programme

            out=-1;

        }else if(strcmp(comm, "FoulRiTaRdE")==0){       

            nobody=-1;

        }
        

    }

    /*printf("________ASM_MIPS________\n");
    printf("    Enter a filename\n\n");
    printf("filename:");*/
    
    //mvwprintw(CMD, ++lns, 1, "filename:");
    //scanf("%s",sourcefile);
    //mvwscanw(CMD, lns,11,"%s %s",sourcefile,sourcefile2);
    /*
    if (parse_file(sourcefile, Memory) == -1){
        mvwprintw(CMD, lns+=2, 1, "!ERROR!file opening not possible");
    }else{
        mvwprintw(CMD, lns+=2, 1, "output in out.hex");
    }*/

    endwin(); // fin de l'interface graphique et retour au terminal classique 

    AhA(nobody);
    
    return 0;

}

void resize_handle(int dummy){

    //endwin();
    //refresh();
    //clear();
    is_resized = 1;
}